import org.jooq.meta.jaxb.Logging
import org.jooq.meta.jaxb.Property
import org.jooq.meta.jaxb.Matchers
import org.jooq.meta.jaxb.MatchersTableType
import org.jooq.meta.jaxb.MatcherRule
import org.jooq.meta.jaxb.MatcherTransformType
import nu.studer.gradle.jooq.JooqGenerate
import java.time.Instant
import java.io.File

buildscript {
  repositories {
    mavenCentral()
  }
  // needed this to be added to the classpath to avoid the following build error.
  // No database found to handle jdbc:mysql://192.168.1.208:3306/classicmodels?createDatabaseIfNotExist=true
  dependencies{
    classpath("org.flywaydb:flyway-mysql:9.18.0")
  }
}

plugins {
	id("java")
	id("org.springframework.boot") version "3.1.2"
	id("io.spring.dependency-management") version "1.1.2"
	kotlin("jvm") version "1.9.0"
	kotlin("plugin.spring") version "1.6.0"
	id("nu.studer.jooq") version "8.0"
	id("org.flywaydb.flyway") version "9.18.0"
}

group = "ajar.ieds"
version = "1.0.0"

val schemaVersion by extra {"1"}

java {
	sourceCompatibility = JavaVersion.VERSION_17
	targetCompatibility = JavaVersion.VERSION_17
}

configurations {
	compileOnly {
		extendsFrom(configurations.annotationProcessor.get())
	}
}

tasks.withType<Copy> {
  duplicatesStrategy = DuplicatesStrategy.INCLUDE
}

repositories {
  maven() {
    url = uri("http://192.168.4.25:8180/repository/public-repositories")
    isAllowInsecureProtocol=true
  }
}

tasks {
  "processResources"(ProcessResources::class) {
    filesMatching("application.yml") {
      expand(project.properties)
    }
  }
}

sourceSets.main {
   java.srcDir("src/main/java")
	 java.srcDir("src/generated/java")
   resources.srcDirs("src/main/resources")
}

sourceSets.test {
  java.srcDirs("src/test/java")
  resources.srcDirs("src/test/resources")
}


tasks.bootRun {
	mainClass.set("ajar.ieds.calc.api.ExpressionApiApplication")
}

dependencies {
	implementation("org.tensorflow:tensorflow-core-api:0.5.0")
  implementation("org.tensorflow:tensorflow-core-platform:0.5.0")
	implementation("ajar.ieds:calc_engine:1.0.0"){
	  exclude("org.slf4","slf4j-api")
    exclude("org.slf4j","slf4j-log4j12")
    exclude("log4j","log4j")
	}
	//implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-jdbc")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-freemarker")
	compileOnly("org.projectlombok:lombok")
	compileOnly("org.springframework.boot:spring-boot-devtools")
	//runtimeOnly("com.mysql:mysql-connector-j")
	implementation("mysql:mysql-connector-java:8.0.33")
	annotationProcessor("org.projectlombok:lombok")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	
	//jooq dependencies
	jooqGenerator("mysql:mysql-connector-java:8.0.33")
	implementation("org.jooq:jooq:3.18.4")
	implementation("org.jooq:jooq-codegen:3.18.4")
	jooqGenerator("org.jooq:jooq-meta-extensions")
	implementation("org.flywaydb:flyway-core")
	implementation("jakarta.validation:jakarta.validation-api:3.0.2")
}

flyway {
	driver = project.properties["driverClassName"].toString()
	url = project.properties["url"].toString()
	user = project.properties["username"].toString()
	password = project.properties["password"].toString()
	locations = arrayOf("filesystem:src/db/sql", "filesystem:src/db/data")
}

jooq {
	version.set(project.properties["jooq"].toString())
	edition.set(nu.studer.gradle.jooq.JooqEdition.OSS)
	configurations {
		create("main") { // name of the jOOQ configuration
			generateSchemaSourceOnCompilation.set(true) // default can be ommitted
			jooqConfiguration.apply {
				logging = Logging.WARN
				generator.apply {
					// The default code generator.
					// You can override this one, to generate your own code style.
					name = "org.jooq.codegen.JavaGenerator"
					database.apply {
						// Rely on jOOQ DDL Database API.
						name = "org.jooq.meta.extensions.ddl.DDLDatabase"
						// name = "org.jooq.meta.mysql.MySQLDatabase"
						// H2 database schema
						inputSchema = "PUBLIC"
						// Specify the location of your SQL script.file
						// You may use ant-style file matching, e.g. /path/**/to/*.sql
						//
						//Where:
						// - ** matches any directory subtree
						// -- * matches any number of characters in a directory /file name
						// - ? matches a single character in a directory/ file name
						properties.add(Property().withKey("scripts").withValue("src/db/sql"))
						// -semantic: sorts versions, e.g. v-3.10.0 is after v-3.9.0 (default)
						// -alphanumeric: sorts string, e.g. v-3.10.0 is before v-3.9.0
						// flyway: sorts files the same way as flyway does.
						// none: doesn't sort directory contents after fetching them from the directory
						properties.add(Property().withKey("sort").withValue("semantic"))
						// The default schema for unqualified objects:
						// -public: all unqualified objects are located in the PUBLIC (upper case) schema
						// -none: all unqualified objects are located in the default schema
						// This configuration can be overridden with the schema mapping feature
						properties.add(Property().withKey("unqualifiedSchema").withValue("none"))
						//The default name case for unquoted objects:
						// -as_is: unquoted object names are kept unquoted
						// -upper: unquoted object names are turned into upper case (most databases)
						// -lower: unquoted object names are turned into lower case (e.g. PostgreSQL)
						properties.add(Property().withKey("defaultNameCase").withValue("as_is"))
						// All elements that are generated from your schema
						// (A Java regular expression. Use the pipe to separate several expressions)
						// Watch out for case-sensitivity. Depending on your database, this might be important! 
						// You can create case-insensitive regular expressions using this syntax: (?i:expr).
						// Whitespace is ignored and comments are possible.
						includes = ".*"                        
						// All elements that are excluded from your schema
						// (A Java regular expression. Use the pipe to separate several expressions).
						// Excludes match before includes, i.e. excludes have a higher priority.
						excludes = """
												flyway_schema_history | sequences 
											| customer_pgs | refresh_top3_product
											| sale_.* | set_.* | get_.* | .*_master
											"""
											
						// Schema version provider
						schemaVersionProvider=schemaVersion
					}
					generate.apply {
						isDeprecated = false
						isRecords = true
						isDaos = true
						isValidationAnnotations = true
						isSpringAnnotations = true
					}
					strategy.withMatchers(Matchers().withTables(arrayOf(
						MatchersTableType()
						.withPojoClass(MatcherRule()
						.withExpression("Jooq_$0")
						.withTransform(MatcherTransformType.PASCAL)),
						MatchersTableType()
						.withDaoClass(MatcherRule()
						.withExpression("$0_Repository")
						.withTransform(MatcherTransformType.PASCAL))
					).toList()))
					target.apply { 
						packageName = "ajar.ieds.calc.api.sql"
						directory = "src/generated/java"
					 }
				}
			}
		}
	}
}

// Configure JOOQ task such that it only executes when something has changed.
// that potentially affects the generated JOOQ sources.
// - the JOOQ configuration has changed (Jdbc, Generator, Strategy, etc.)
// (JOOQ library, database driver, strategy classes etc.)
// - the schema files from which the schema is generated and whic is 
// used by jOOQ to generate the sources have chnaged (scripts added, modified, etc.)
tasks.named<JooqGenerate>("generateJooq") {
	// ensure database schema has been prepared by Flyway before generating the jOOQ sources
	dependsOn("flywayMigrate")
	//declare flyway migration scripts as inputs on the JOOQ task
	project.logger.info("${rootDir}/src/db")
	inputs.files(fileTree("${rootDir}/src/db"))
	.withPropertyName("migrations")
	.withPathSensitivity(PathSensitivity.RELATIVE)
	// MAE jOOQ task participate in incremental builds and build caching
	allInputsDeclared.set(true)
	outputs.cacheIf {true}
}
tasks.withType<Test> {
	useJUnitPlatform()
}

// trigger restart of the server on changes to trigger file.
task("finalize") {
	doLast {
			println("Writing devtools restart trigger file")
			File("${rootDir}/trigger-restart/trigger.txt").writeText(Instant.now().toString())
	}
}
tasks.compileJava {
	finalizedBy("finalize")
}
// Apply a specific Java toolchain to ease working on different environments.
java {
  toolchain {
      languageVersion.set(JavaLanguageVersion.of(20))
  }
}