/*
 * This file is generated by jOOQ.
 */
package ajar.ieds.calc.api.sql.tables;


import ajar.ieds.calc.api.sql.DefaultSchema;
import ajar.ieds.calc.api.sql.Keys;
import ajar.ieds.calc.api.sql.tables.records.ExpressionRecord;

import java.time.LocalDateTime;
import java.util.function.Function;

import javax.annotation.processing.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Function6;
import org.jooq.Identity;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Records;
import org.jooq.Row6;
import org.jooq.Schema;
import org.jooq.SelectField;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.18.4",
        "schema version:1"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Expression extends TableImpl<ExpressionRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>expression</code>
     */
    public static final Expression EXPRESSION = new Expression();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<ExpressionRecord> getRecordType() {
        return ExpressionRecord.class;
    }

    /**
     * The column <code>expression.expression_id</code>.
     */
    public final TableField<ExpressionRecord, Long> EXPRESSION_ID = createField(DSL.name("expression_id"), SQLDataType.BIGINT.nullable(false).identity(true), this, "");

    /**
     * The column <code>expression.name</code>.
     */
    public final TableField<ExpressionRecord, String> NAME = createField(DSL.name("name"), SQLDataType.VARCHAR(20), this, "");

    /**
     * The column <code>expression.expression</code>.
     */
    public final TableField<ExpressionRecord, String> EXPRESSION_ = createField(DSL.name("expression"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>expression.created_at</code>.
     */
    public final TableField<ExpressionRecord, LocalDateTime> CREATED_AT = createField(DSL.name("created_at"), SQLDataType.LOCALDATETIME(6), this, "");

    /**
     * The column <code>expression.modified_at</code>.
     */
    public final TableField<ExpressionRecord, LocalDateTime> MODIFIED_AT = createField(DSL.name("modified_at"), SQLDataType.LOCALDATETIME(6), this, "");

    /**
     * The column <code>expression.author</code>.
     */
    public final TableField<ExpressionRecord, String> AUTHOR = createField(DSL.name("author"), SQLDataType.VARCHAR(20), this, "");

    private Expression(Name alias, Table<ExpressionRecord> aliased) {
        this(alias, aliased, null);
    }

    private Expression(Name alias, Table<ExpressionRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    /**
     * Create an aliased <code>expression</code> table reference
     */
    public Expression(String alias) {
        this(DSL.name(alias), EXPRESSION);
    }

    /**
     * Create an aliased <code>expression</code> table reference
     */
    public Expression(Name alias) {
        this(alias, EXPRESSION);
    }

    /**
     * Create a <code>expression</code> table reference
     */
    public Expression() {
        this(DSL.name("expression"), null);
    }

    public <O extends Record> Expression(Table<O> child, ForeignKey<O, ExpressionRecord> key) {
        super(child, key, EXPRESSION);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : DefaultSchema.DEFAULT_SCHEMA;
    }

    @Override
    public Identity<ExpressionRecord, Long> getIdentity() {
        return (Identity<ExpressionRecord, Long>) super.getIdentity();
    }

    @Override
    public UniqueKey<ExpressionRecord> getPrimaryKey() {
        return Keys.EXPRESSION_PK;
    }

    @Override
    public Expression as(String alias) {
        return new Expression(DSL.name(alias), this);
    }

    @Override
    public Expression as(Name alias) {
        return new Expression(alias, this);
    }

    @Override
    public Expression as(Table<?> alias) {
        return new Expression(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public Expression rename(String name) {
        return new Expression(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Expression rename(Name name) {
        return new Expression(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public Expression rename(Table<?> name) {
        return new Expression(name.getQualifiedName(), null);
    }

    // -------------------------------------------------------------------------
    // Row6 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row6<Long, String, String, LocalDateTime, LocalDateTime, String> fieldsRow() {
        return (Row6) super.fieldsRow();
    }

    /**
     * Convenience mapping calling {@link SelectField#convertFrom(Function)}.
     */
    public <U> SelectField<U> mapping(Function6<? super Long, ? super String, ? super String, ? super LocalDateTime, ? super LocalDateTime, ? super String, ? extends U> from) {
        return convertFrom(Records.mapping(from));
    }

    /**
     * Convenience mapping calling {@link SelectField#convertFrom(Class,
     * Function)}.
     */
    public <U> SelectField<U> mapping(Class<U> toType, Function6<? super Long, ? super String, ? super String, ? super LocalDateTime, ? super LocalDateTime, ? super String, ? extends U> from) {
        return convertFrom(toType, Records.mapping(from));
    }
}
