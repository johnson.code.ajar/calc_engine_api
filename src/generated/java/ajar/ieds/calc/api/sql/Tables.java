/*
 * This file is generated by jOOQ.
 */
package ajar.ieds.calc.api.sql;


import ajar.ieds.calc.api.sql.tables.Expression;

import javax.annotation.processing.Generated;


/**
 * Convenience access to all tables in the default schema.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.18.4",
        "schema version:1"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tables {

    /**
     * The table <code>expression</code>.
     */
    public static final Expression EXPRESSION = Expression.EXPRESSION;
}
