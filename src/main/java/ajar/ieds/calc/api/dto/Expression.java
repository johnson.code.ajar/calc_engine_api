package ajar.ieds.calc.api.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Expression {
  private String expression;
  private String name;
  private LocalDateTime modifiedAt;
  private LocalDateTime createdAt;
  private String author;
}
