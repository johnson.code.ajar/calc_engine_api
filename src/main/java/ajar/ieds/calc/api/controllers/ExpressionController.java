package ajar.ieds.calc.api.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import ajar.ieds.calc.api.dto.Expression;
import ajar.ieds.calc.api.services.ExpressionService;

@Controller
public class ExpressionController {
  @Autowired
  private ExpressionService service;

  @GetMapping("/info")
  public String info() {
    return "Expression Api.";
  }

  @GetMapping("/expressions")
  public List<Expression> getExpressions() {
    return service.getExpressions();
  }

  

  @GetMapping("/expressions/{name}")
  public Expression getExpression(@PathVariable("name") String name) {
    return service.getExpression(name);
  }

  @PutMapping(value="/expressions/{name}", consumes = "application/json", produces = "application/json")
  public void updateExpression(@PathVariable("name") String name, String expression) {
    service.updateExpression(name, expression);
  }

  @DeleteMapping(value="/expressions/{name}", consumes = "application/json")
  public void deleteExpression(@PathVariable("name") String name) {
    
  }

  @PostMapping(value="/expressions/{exprName}/run")
  public void runExpression() {
    
  }
}
