package ajar.ieds.calc.api.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ajar.ieds.calc.api.dto.Expression;
import ajar.ieds.calc.api.services.ExpressionService;

@Controller("/ui")
public class ExpressionMvcController {
  @Value("${msg.title}")
  private String title;
  
  private final int ROW_PER_PAGE = 5;
  
  @Autowired
  private ExpressionService service;
  
  @GetMapping(value = {"/ui", "/index"})
  public String index(Model model) {
    System.out.println("Calling index page2");
    model.addAttribute("title", title);
    return "index";
  }

  // TODO: Handle pagination.
  @GetMapping(value = {"/ui/expressions"})
  public String getExpressions(Model model){
    List<Expression> expressions = service.getExpressions();
    model.addAttribute("title", title);
    model.addAttribute("expressions", expressions);
    return "expressionList";
  }

  @GetMapping(value={"/ui/expressions/pages"})
  public String getExpressionByPage(Model model,  @RequestParam(value="page", defaultValue="1") int pageNumber){
    long count = service.count();
    boolean hasPrev = pageNumber > 1;
    boolean hasNext = (pageNumber * ROW_PER_PAGE) < count;
    model.addAttribute("hasPrev", hasPrev);
    model.addAttribute("prev", pageNumber - 1);
    model.addAttribute("hasNext", hasNext);
    model.addAttribute("next", pageNumber+1);
    return "expressionList";
  }

  @GetMapping("/ui/expressions/{name}")
  public String getExpression(Model model, @PathVariable(value="name") String name){
    Expression expression = service.getExpression(name);
    model.addAttribute("title", title);
    model.addAttribute("expression", expression);
    model.addAttribute("allowDelete", false);
    return "showExpression";
  }

  @GetMapping("/ui/expression/{name}/edit")
  public String editExpressionPage(Model model, @PathVariable(value="name") String name){
    Expression expression = service.getExpression(name);
    model.addAttribute("title", title);
    model.addAttribute("expression", expression);
    model.addAttribute("add", false);
    return "editExpression";
  }

  @PostMapping("/ui/expressions/{name}/edit")
  public String updateExpression(Model model, @PathVariable(value="name") String name) {
    Expression expression = service.getExpression(name);
    model.addAttribute("title", title);
    model.addAttribute("expression", expression);
    return "redirect:/ui/expressions";
  }

  @GetMapping("/ui/expressions/{name}/delete")
  public String showDeleteExpressionPage(Model model, @PathVariable(value="name") String name) {
    Expression expression = service.getExpression(name);
    model.addAttribute("title", title);
    model.addAttribute("allowDelete", true);
    model.addAttribute("expression", expression);
    return "showExpression";
  }

  @PostMapping("/ui/expressions/{name}/delete")
  public String deleteExpression(Model model, @PathVariable(value="name") String name) {
    service.deleteExpression(name);
    return "redirect:/ui/expressions";
  }
}
