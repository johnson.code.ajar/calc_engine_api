package ajar.ieds.calc.api.services;
import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ajar.ieds.calc.api.dto.Expression;
import ajar.ieds.calc.api.sql.Tables;
import ajar.ieds.calc.api.sql.tables.daos.ExpressionRepository;
import ajar.ieds.calc.api.sql.tables.pojos.JooqExpression;

@Service
public class ExpressionService {
  
  @Autowired
  private ExpressionRepository repository;

  private Expression toExpression(JooqExpression jExpr) {
    return Expression.builder()
      .expression(jExpr.getExpression())
      .name(jExpr.getName())
      .author(jExpr.getAuthor())
      .createdAt(jExpr.getCreatedAt())
      .modifiedAt(jExpr.getModifiedAt())
      .build();
  }

  public long count() {
    return repository.count();
  }

  public List<Expression> getExpressions() {
    //TODO: Handle pagination
    return repository.findAll().stream().map(jExpr -> toExpression(jExpr) ).toList();
  }

  public Expression getExpression(String name) {
    System.out.println(name);
    return toExpression(repository.ctx()
    .selectFrom(table(Tables.EXPRESSION.getName()))
    .where(field(Tables.EXPRESSION.NAME.getName()).eq(name)).fetchOne().into(JooqExpression.class));
  }

  public void updateExpression(String name, String expression) {
    //repository.ctx().selectFrom(table(Tables.EXPRESSION.getName()).eq(name)).u
    repository.ctx().update(table(Tables.EXPRESSION.getName())).set(field(Tables.EXPRESSION.EXPRESSION_.getName()), expression).execute();
  }

  public void deleteExpression(String name) {
    repository.ctx().deleteFrom(table(Tables.EXPRESSION.getName())).where(field(Tables.EXPRESSION.EXPRESSION_.getName()).eq(name)).execute();
  }
}
