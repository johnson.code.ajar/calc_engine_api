package ajar.ieds.calc.api;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExpressionApiApplication {
  public static void main(String[] args) {
    SpringApplication.run(ExpressionApiApplication.class, args);
  }
}