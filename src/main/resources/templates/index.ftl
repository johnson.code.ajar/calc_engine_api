<#import "libs/utils.ftl" as u>
<@u.page>
   <@u.header>
      <div class="page-links">
          <a href="/ui/expressions">Expressions</a>
      </div>
  </@u.header>
  
  <@u.body>
    <span>Body</span>
  </@u.body>
  <@u.footer/>
</@u.page>