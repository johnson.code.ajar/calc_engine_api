<#macro header>
  <div class="header">
    <h1>${title}</h1>
    <#nested>
  </div>
</#macro>
<#macro body>
  <div class="body">
    <#nested>
  </div>
</#macro>

<#macro footer>
  <div class="footer">
    <#nested>
    <span>Copyright1 © ajar.com</span>
  </div>
</#macro>

<#macro page>
  <html>
    <head>
      <title>${title}</title>
      <link rel="stylesheet" type="text/css" href="/css/style.css"/>
    </head>
    <body class="main">
      <#nested>
    </body>
  </html>
</#macro>
