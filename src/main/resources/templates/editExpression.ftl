<#import "libs/utils.ftl" as u>
<@u.page>
  <@u.header>
    <a href="/ui/expressions/ui">Home</a> |
    <a href="/ui/expressions/add">Add</a>
  </@u.header>
  <@u.body>
    <#if expression??>
      <#assign urlAction>${'/ui/expressions/'+expression.name+'/edit'}</#assign>
      <#assign submitTitle>Update</#assign>
      <div>
        <h3>Edit Expression: ${expression.name}</h3>
        <form action="${urlAction}" name="expression" method="POST">
          <table border="0">
          <tr>
            <td>Name</td>
            <td>:</td>
            <td><input type="text" id="expression_name" name="expression_name" value="${expression.name}!''"/></td>
          </tr>
          <tr>
            <td>Expression</td>
            <td>:</td>
            <td><textarea name="expression" rows="4" cols="50">${(expression.expression)!""}</textarea></td>
          </tr>
          <tr>
            <td>Created At</td>
            <td>:</td>
            <td>${expression.createdAt}</td>
          </tr>
          <tr>
            <td>Modified At</td>
            <td>:</td>
            <td>${expression.modifiedAt}</td>
          </tr>
          <tr>
            <td>Author</td>
            <td>:</td>
            <td>${expression.author}</td>
          </tr>
          </table>
          <input type="submit" value="${submitTitle}" />
        </form>
      </div>
    </#if>
  </@u.body>
</@u.page>