<#import "libs/utils.ftl" as u>
<@u.page>
  <@u.header>
    <div class="page-links">
      <a href="/ui/expressions">Expressions</a> |
      <a href="/ui/expressions/${expression.name}/update">Edit</a>
    </div>
  </@u.header>
  <@u.body>
    <#if expression??>
      <div>
        <h3>Details of Expression : ${expression.name}</h3>
        <table border="0" class="table">
          <tr>
            <td>Name</td>
            <td>:</td>
            <td>${expression.name}</td>
          </tr>
          <tr>
            <td>Expression</td>
            <td>:</td>
            <td>${expression.expression}</td>
          </tr>
          <tr>
            <td>Create At</td>
            <td>:</td>
            <td>${expression.createdAt}</td>
          </tr>
          <tr>
            <td>Modified At</td>
            <td>:</td>
            <td>${expression.modifiedAt}</td>
          </tr>
          <tr>
            <td>Author</td>
            <td>:</td>
            <td>${expression.author}</td>
          </tr>
        </table>
        <#if allowDelete>
          <form action="${'/ui/expressions/'+expression.name+'/delete'}" method="POST">
            Delete this Expression ? <input type="submit" value="Yes"/>
          </form>
        <#else>
          <div>
            <a href="${'/ui/expressions/'+expression.name+'/edit'}">Edit</a>
            <a href="${'/ui/expressions/'+expression.name+'/delete'}">Delete</a>
          </div>
        </#if>
      </div>
    </#if>
  </@u.body>
</@u.page>