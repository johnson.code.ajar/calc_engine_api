<#import "libs/utils.ftl" as u>
<@u.page>
  <@u.header>
    <div class="page-links">
      <a href="/ui/expressions/add">Add Expression</a> |
      <a href="/ui">Home</a>
    </div>
  </@u.header>
  <@u.body>
    <table border="1" class="table">
      <tr class="tr">
        <th>Name</th>
        <th>Expression</th>
        <th>Created At</th>
        <th>Modified At </th>
        <th>Author</th>
      </tr>
      <#list expressions as expression>
        <tr class="tr">
          <td><a href="/ui/expressions/${expression.name}">${expression.name}</a></td>
          <td>${expression.expression}</td>
          <td>${expression.createdAt}</td>
          <td>${expression.modifiedAt}</td>
          <td>${expression.author}</td>
        </tr>
      </#list>
    </table>
  </@u.body>
  <@u.footer/>
</@u.page>