package ajar.ieds.calc.api;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = {ExpressionApiApplication.class})
class ExpressionApiApplicationTests {

	@Test
	void contextLoads() {
	}

}
