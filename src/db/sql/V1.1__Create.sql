drop table if exists `expression`;
CREATE TABLE `expression` (
  `expression_id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(20),
  `expression` TEXT,
  `created_at` TIMESTAMP,
  `modified_at` TIMESTAMP,
  `author` VARCHAR(20),
  CONSTRAINT `expression_pk` PRIMARY KEY (`expression_id`)
 ) ENGINE = InnoDB DEFAULT CHARSET=latin1;