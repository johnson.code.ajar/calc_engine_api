insert into `expression`(`name`,`expression`, `created_at`, `modified_at`, `author`) values ('expr1', 'a=10;b=20;c=a+b;', CURDATE(),CURDATE(), 'Johnson Abraham'); 
insert into `expression`(`name`,`expression`, `created_at`, `modified_at`, `author`) values ('expr2', 'a=10;b=20;c=a-b;', CURDATE(),CURDATE(), 'Johnson Abraham');
insert into `expression`(`name`,`expression`, `created_at`, `modified_at`, `author`) values ('expr3', 'a=10;b=20;c=a*b;', CURDATE(),CURDATE(), 'Johnson Abraham');
insert into `expression`(`name`,`expression`, `created_at`, `modified_at`, `author`) values ('expr4', 'a=10;b=20;c=a/b;', CURDATE(),CURDATE(), 'Johnson Abraham');
insert into `expression`(`name`,`expression`, `created_at`, `modified_at`, `author`) values ('expr5', 'a=[10,10];b=[20,20];c=a+b;', CURDATE(),CURDATE(), 'Paul');


insert into `expression`(`name`,`expression`, `created_at`, `modified_at`, `author`) values ('expr6', 'a=[10,10];b=[20,20];c=a-b;', CURDATE(),CURDATE(), 'Paul'); 

insert into `expression`(`name`,`expression`, `created_at`, `modified_at`, `author`) values ('expr7', 'a=[10,10];b=[20,20];c=a*b;', CURDATE(),CURDATE(), 'Paul'); 

insert into `expression`(`name`,`expression`, `created_at`, `modified_at`, `author`) values ('expr8', 'a=[10,10];b=[20,20];c=a/b;', CURDATE(),CURDATE(), 'Paul'); 